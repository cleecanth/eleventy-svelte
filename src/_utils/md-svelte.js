const attr_name = '[a-zA-Z_:][a-zA-Z0-9:._-]*';

const unquoted = '[^"\'=<>`\\x00-\\x20]+';
const single_quoted = "'[^']*'";
const double_quoted = '"[^"]*"';

const attr_value =
'(?:' + unquoted + '|' + single_quoted + '|' + double_quoted + ')';

const attribute = '(?:\\s+' + attr_name + '(?:\\s*=\\s*' + attr_value + ')?)';

// this is what was modified, it seems to pass tests without breaking anything
const open_tag =
'<[A-Za-z][A-Za-z0-9\\-]*:*[a-zA-Z]+' + attribute + '*\\s*\\/?>';

const close_tag = '<\\/[A-Za-z][A-Za-z0-9\\-]*\\s*>';

const HTML_OPEN_CLOSE_TAG_RE = new RegExp(
    '^(?:' + open_tag + '|' + close_tag + ')'
    );
    
const svelteBlock = [
    [/^<(svelte:head)(?=(\s|>|$))/i, /<\/(svelte:head)>/i, true],
    [new RegExp(HTML_OPEN_CLOSE_TAG_RE.source + '\\s*$'), /^$/, false],
];

function replaceCurlies(str) {
    return str.replace(/{/g, '&#123;').replace(/}/g, '&#125;');
}
  
function escapeCurly(md) {
    const default_code_inline = md.renderer.rules.code_inline;
    const default_code_block = md.renderer.rules.code_block;
    const default_fence = md.renderer.rules.fence;
  
    md.renderer.rules.code_block = function(tokens, idx, options, env, slf) {
      return replaceCurlies(default_code_block(tokens, idx, options, env, slf));
    };
    md.renderer.rules.code_inline = function(tokens, idx, options, env, slf) {
      return replaceCurlies(default_code_inline(tokens, idx, options, env, slf));
    };
    md.renderer.rules.fence = function(tokens, idx, options, env, slf) {
      return replaceCurlies(default_fence(tokens, idx, options, env, slf));
    };
}

function svelte_block(state, startLine, endLine, silent) {
    var i,
    nextLine,
    token,
    lineText,
    pos = state.bMarks[startLine] + state.tShift[startLine],
    max = state.eMarks[startLine];
    
    if (state.sCount[startLine] - state.blkIndent >= 4) {
        return false;
    }
    
    if (!state.md.options.html) {
        return false;
    }
    
    if (state.src.charCodeAt(pos) !== 0x3c /* < */) {
        return false;
    }

    
    lineText = state.src.slice(pos, max);
    
    for (i = 0; i < svelteBlock.length; i++) {
        //@ts-ignore
        if (svelteBlock[i][0].test(lineText)) {
            break;
        }
    }
    
    if (i === svelteBlock.length) {
        return false;
    }
    
    if (silent) {
        return svelteBlock[i][2];
    }
    
    nextLine = startLine + 1;
    
    
    if (!svelteBlock[i][1].test(lineText)) {
        for (; nextLine < endLine; nextLine++) {
            if (state.sCount[nextLine] < state.blkIndent) {
                break;
            }
            
            pos = state.bMarks[nextLine] + state.tShift[nextLine];
            max = state.eMarks[nextLine];
            lineText = state.src.slice(pos, max);
            
            if (svelteBlock[i][1].test(lineText)) {
                if (lineText.length !== 0) {
                    nextLine++;
                }
                break;
            }
        }
    }
    
    state.line = nextLine;
    
    token = state.push('svelte_block', '', 0);
    token.map = [startLine, nextLine];
    token.content = state.getLines(startLine, nextLine, state.blkIndent, true);
    
    return true;
}
    
// the renderer just returns the raw string
function svelteRenderer(tokens, idx) {
    return tokens[idx].content;
}
    
function svelte(md) {
    md.block.ruler.before('table', 'svelte_block', svelte_block);
    md.renderer.rules['svelte_block'] = svelteRenderer;
}

module.exports = { svelte, escapeCurly };