require('svelte/ssr/register')({ hydratable: true });
const svelte = require('svelte');
const app = require('../components/Router.html');
const base = require('./base.js');
const fs = require('fs');

module.exports = (data) => {

    function readComponent(component) {
        const path = require.resolve('../components/' + component);
        const str = fs.readFileSync(path, 'utf8', (err, data) => data);
        return svelte.create(str, {generate:'ssr', hydrateable: true });
    }

    if (data.imports) {
        console.log(data.imports);
        data.imports = Array.isArray(data.imports) 
            ? data.imports.map(  
                  (component) => readComponent(component)
              )
            : readComponent(data.imports);
        
        console.log('ssr', data.imports);
    }

    data.content = svelte.create(
        data.content, 
        { generate: 'ssr', hydratable: true }
    );
        
    const render = app.render(data);

    delete require.cache[require.resolve('../components/Router.html')];
    delete require.cache[require.resolve('./base.js')];
    return base(render);
};