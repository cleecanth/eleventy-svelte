module.exports = ({head, html, css}) => `
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    ${head}
</head>
<body>
    ${html}
</body>
</html>`;
