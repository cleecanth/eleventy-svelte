module.exports = {
    "presets": [
        ["@babel/preset-env", {
        modules: "cjs",
        loose: true,
    }]],
    "plugins": [
        ["@babel/plugin-syntax-dynamic-import"],
        ["babel-plugin-dynamic-import-node"]
    ],
}