const path = require('path');
const glob = require('fast-glob');
const PATHS = {
    src: path.resolve('./src'),
    dist: path.resolve('./11ty'),
    layouts: path.resolve('./src/_includes/**.js')
};

module.exports = {
    mode: 'development',
    devtool: false,
    target: 'node',
    resolve: {
        alias: {
            '@': PATHS.src,
            'Layouts': PATHS.layouts,
        },
    },
    entry: () => {
        const entries = {
            main: PATHS.src + '/main.js'
        };

        glob.sync(PATHS.layouts).forEach((file) => {
            const baseName = path.basename(file, '.html');
            const entryName = path.posix.join('_includes', baseName);
            entries[entryName] = file;
        });

        return entries;
    },
    output: {
        path: PATHS.dist,
        publicPath: '/',
        libraryTarget: 'commonjs2',
        filename: `[name]`,
        // chunkFilename: `[id].js`,
        // hotUpdateChunkFilename: '[id].hot-update.js',
        // hotUpdateMainFilename: 'main.hot-update.js',
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /[\\/]node_modules[\\/]/,
                loader: 'babel-loader'
            },
            {
                test: /\.html$/,
                use: [
                    //'babel-loader',
                    {
                        loader: 'svelte-loader',
                        options: {
                            generate: 'ssr',
                            hydratable: true,
                        }
                    }
                ]
            }
        ]
    }
}