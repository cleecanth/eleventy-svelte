module.exports = (eleventyConfig) => {
    const markdownIt = require("markdown-it");
    const svelte = require('svelte');
    const mdSvelte = require('./src/_utils/md-svelte.js');

    // let options = {
    //     html: true
    // };
    let markdownLib = markdownIt({
        html: true
    })
    .use(mdSvelte.svelte)
    .use(mdSvelte.escapeCurly);

    eleventyConfig.setLibrary("md", markdownLib);

    eleventyConfig.setDataDeepMerge(true);
    eleventyConfig.addLayoutAlias('svelte', 'layouts/svelte.11ty.js');

    return {
        dir: {
            input: "src",
            output: "dist"
        }
    };
}