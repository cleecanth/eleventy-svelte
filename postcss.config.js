module.exports = ({ file, options, env }) => ({
    plugins: {
        'autoprefixer': {grid: "autoplace"},
        'cssnano': env === 'production' ? {} : false,
    }
});